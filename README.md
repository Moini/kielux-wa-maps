## Workadventure maps for Kielux / KITZ

This is a set of Workadventure maps for Kieler Open Source und Linux Tage 2021.

Find photos to guide you here:
https://framadrive.org/s/jGAt45HE9wb2RS3

Find a link to the map here: https://moini.gitlab.io/kielux-wa-maps/Kitz

### License: 

The tiles all come with their own license.
Each tile's license is marked in the corresponding tileset.

Everything that is not a tile is licensed as CC-By-SA 4.0.
