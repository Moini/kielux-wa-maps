# Quellenangaben zu den WorkAdventure-Ressourcen für die Kielux-Karten


## Musik / Geräusche


### birthday.opus

Zusammenschnitt aus:

* [Tröte](https://freesound.org/people/D.jones/sounds/532853/)
* [Menschenmenge ruft "Happy Birthday!"](https://freesound.org/people/theuned/sounds/391869/)
* [Restaurant-Glückwünsche](https://freesound.org/people/phonoflora/sounds/500145/)
* [Geburtstagsgästeunterhaltung](https://freesound.org/people/phonoflora/sounds/500145/) </a>
* [Harfe ("Viel Glück und viel Segen")](https://freesound.org/people/ChaliceWell/sounds/579087/)


Lizenz (einzeln und zusammengeschnitten): [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)

### grillen.mp3

Dies ist eine gekürzte und bearbeitete Version [der Grill-Aufnahmen](https://freesound.org/people/YleArkisto/sounds/272360/)  von Yle (Finnish Broadcasting Company) Archives, Bearbeitung: Maren Hachmann.  
Lizenz: [**CC-By-SA 3.0**](https://creativecommons.org/licenses/by-sa/3.0/deed.de)

### lagerfeuer.mp3
    
Dies ist eine gekürzte und bearbeitete Version von []()https://freesound.org/people/aerror/sounds/350757/ https://freesound.org/people/aerror/sounds/350757/.  
Lizenz: <a target="_blank" href="https://creativecommons.org/publicdomain/zero/1.0/deed.de">**CC-0**</a>


### moewen_vor_dem_Kitz.mp3 

Es handelt sich um einen Zusammenschnitt aus:
    
* https://freesound.org/people/miklovan/sounds/194503/
* https://freesound.org/people/Danjocross/sounds/507471/
* https://freesound.org/people/SiriusParsec/sounds/532092/
* https://freesound.org/people/giddykipper/sounds/53491/
* https://freesound.org/people/TRP/sounds/574276/
* https://freesound.org/people/TRP/sounds/574388/
* https://freesound.org/people/nigelcoop/sounds/73497/
* https://freesound.org/people/CBeeching/sounds/74971/
* https://freesound.org/people/aerror/sounds/350757/

Lizenz (einzeln und Zusammenschnitt): [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)


### party.opus

Zusammenschnitt aus:

* ["Celebrate Software Freedom Day Song (Enhanced Audio version)"](https://www.youtube.com/watch?v=_kTfcfQ3uEo) von Lariz Santos, labs.cre8tivetech.com, members of SFD.PH
* ["Verdächtig"](https://systemabsturz.bandcamp.com/track/verd-chtig) von [Systemabsturz](https://www.systemabsturz.band/)
* ["Daten daten Daten"](https://systemabsturz.bandcamp.com/track/daten-daten-daten) von [Systemabsturz](https://www.systemabsturz.band/)
* ["Netzkater"](https://systemabsturz.bandcamp.com/track/netzkater) von [Systemabsturz](https://www.systemabsturz.band/)


Alle einzeln und als Zusammenschnitt lizenziert als [**CC-By-SA 3.0**](https://creativecommons.org/licenses/by-sa/3.0/deed.de).

### vogelzwitschern.opus

Bearbeitete eigene Aufnahme  
Lizenz: [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)


## Grafiken


### bench.png
Die [Bierzeltgarnitur](https://opengameart.org/content/bench-with-flowers) ist ein Derivat der Bank von Hyptosis und Jordan Irwin (s. attribution.png)  
Lizenz: [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)


### building.png
Das KITZ-Gebäude wurde erstellt von [Maren Hachmann](https://vektorrascheln.de).  
Lizenz: [**CC-By-SA 4.0**](https://creativecommons.org/licenses/by-sa/4.0/deed.de)


### dk_transports.png
[Animierte Teleporter](https://git.cccv.de/rc3/world-tiles/-/tree/master/inventory) vom rc3  
Lizenz: [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)


### entry_and_exit.png
Grafiken von [OpenClipart](https://openclipart.org) bzw. teils erstellt von Maren Hachmann.  
Lizenz: [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)


### floors.png
Dieses Tileset enthält Kacheln unterschiedlichen Ursprungs:

[Rasen / Teich / Pflastersteine](https://opengameart.org/content/lpc-tile-atlas) von [verschiedenen Beitragenden](https://gitlab.com/Moini/kielux-wa-maps/-/blob/main/public/Kitz/tilesets/Attribution_Atlas.txt)  
Duale Lizenz: [**GPL 3.0**](https://www.gnu.org/licenses/gpl-3.0) und [**CC-By-SA 3.0**](https://creativecommons.org/licenses/by-sa/3.0/deed.de)


[Fußbodenbeläge bunt und schwarz](https://github.com/cijavel/tileset_Bytewerk/blob/main/cija_32x32_basic_CC0.png) (Nieten entfernt, Farben angepasst)  
Lizenz: [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)

[Animierte Himmelsobjekte](https://git.cccv.de/rc3/world-tiles/-/blob/master/deco/rc3-mullana-neon-signs.png)  
Lizenz: [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)

Blauer Teppich selbsterstellt  
Lizenz: [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)

[Rote Blümchen im Gras aus dem Atlas-Set](https://opengameart.org/content/lpc-tile-atlas) von [verschiedenen Beitragenden](https://gitlab.com/Moini/kielux-wa-maps/-/blob/main/public/Kitz/tilesets/Attribution_Atlas.txt)  
Duale Lizenz: [**GPL 3.0**](https://www.gnu.org/licenses/gpl-3.0) und [**CC-By-SA 3.0**](https://creativecommons.org/licenses/by-sa/3.0/deed.de)


### tables_&_stools-white.png
[Weiße Tische und Stühle](https://opengameart.org/node/20715)  
Duale Lizenz: [**CC-By 3.0**](https://creativecommons.org/licenses/by/3.0/deed.de) und [https://opengameart.org/content/oga-by-30-faq](OGA-BY 3.0)


### food_and_drinks.png
[Matekisten, -flaschen und einige andere Lebensmittel](https://github.com/c3CERT/rc3_tiles/tree/master/imgs/food_and_drinks) stammen vom [CERT](https://cert.ccc.de), einige der Gegenstände wurden nachträglich neu ausgerichtet.  
Lizenz: [**CC-By-SA 4.0**](https://creativecommons.org/licenses/by-sa/4.0/deed.de)


### furniture.png
[Sitzsäcke](https://github.com/c3CERT/rc3_tiles/tree/master/imgs/misc) stammen vom [CERT](https://cert.ccc.de).  
Lizenz: [**CC-By-SA 4.0**](https://creativecommons.org/licenses/by-sa/4.0/deed.de)


### groundTransparent_white.png
[Sportplatzdeko](https://kenney.nl/assets/sports-pack)</a> von kenney.nl  
Lizenz: [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)

### hexenkreise.png
Grafik erstellt von Maren Hachmann.  
Lizenz: [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)


### logos.png
Logos von [Toppoint (entworfen von katja@bitbackofen)](https://toppoint.de/verein/wettbewerbe/logo/start)</a> und vom [Chaotikum](https://wiki.chaotikum.org/hackspace:ci) in Lübeck.


### möve.png
Grafiken erstellt von [Kate](https://codeberg.org/kate/PixelArt).  
Das KITZ-Logo ist Eigentum des KITZ.  
Lizenz: [**CC-By-SA 4.0**](https://creativecommons.org/licenses/by-sa/4.0/deed.de)


### NewCampFire_0.png
Das [animierte Lagerfeuer](https://opengameart.org/content/camp-fire-animation-for-rpgs-finished) wurde erstellt von Zabin und Jetrel von OpenGameArt.  
Lizenz: [**CC-By-SA 3.0**](https://creativecommons.org/licenses/by-sa/3.0/deed.de)


### Outside Objects.png

Diese Datei ist eine Zusammestellung aus mehreren Quellen.

Der [größte Teil](https://opengameart.org/content/lpc-submissions-merged) stammt aus einem Set [von verschiedenen Urhebern](https://gitlab.com/Moini/kielux-wa-maps/-/tree/main/public/Kitz/tilesets/LPC_submissions_merged_2.0)  
Lizenz: [**GPL 3.0**](https://www.gnu.org/licenses/gpl-3.0) und [**CC-By-SA 3.0**](https://creativecommons.org/licenses/by-sa/3.0/deed.de) 

Kissen und Hochbeet modifiziert (aus Sack, Wandpaneelen und Erdboden) von [Maren Hachmann](https://vektorrascheln.de)

[Seerosen, Schilf, Erde und Gemüse aus dem Atlas-Set](https://opengameart.org/content/lpc-tile-atlas) von [verschiedenen Beitragenden](https://gitlab.com/Moini/kielux-wa-maps/-/blob/main/public/Kitz/tilesets/Attribution_Atlas.txt)  
Duale Lizenz: [**GPL 3.0**](https://www.gnu.org/licenses/gpl-3.0) und [**CC-By-SA 3.0**](https://creativecommons.org/licenses/by-sa/3.0/deed.de)

Weitere Gegenstände (Blumen, Schild, Vogelhaus, Lebensmitel, Mauer, hohle Baumstämme, Sack, Fass, Hocker, Stein, Marktstand, buschiger Baum) stammen aus [einem Erweiterungsset](https://github.com/cijavel/tileset_Bytewerk/blob/main/cija_32x32_expansion%20for%20Pipoya_CC0.png)  
Lizenz: [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)

### pastry.png

Zusammengestellt von [Maren Hachmann](https://vektorrascheln.de) aus:


* Geburtstagstorte von [Michael J Pierce @ IsometricRobot.com](https://IsometricRobot.com)  
    Lizenz: [**CC-By 3.0**](https://creativecommons.org/licenses/by/3.0/deed.de)
* [Gebäckstücke](https://opengameart.org/content/lpc-food) – 
    Auszug aus "[LPC] Foods" von bluecarrot16, Daniel Eddeland (daneeklu), Joshua Taylor, Richard Kettering (Jetrel), thekingphoenix, RedVoxel, and Molly "Cougarmint" Willits. Siehe [Datei](https://gitlab.com/Moini/kielux-wa-maps/-/tree/main/public/Kitz/tilesets/pastry/License.txt)  
    Duale Lizenz: [**CC-By-SA 3.0**](https://creativecommons.org/licenses/by-sa/3.0/deed.de) und [**GPL 3.0**](https://www.gnu.org/licenses/gpl-3.0).
* [Kuchenstück mit Kerze](https://opengameart.org/content/16x16-pixel-cake)  
    Lizenz: [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)

Lizenz (zusammen): [**CC-By-SA 3.0**](https://creativecommons.org/licenses/by-sa/3.0/deed.de)


### pinboard_blue.png
Grafiken erstellt von Maren Hachmann.  
Lizenz: [**CC-By-SA 4.0**](https://creativecommons.org/licenses/by-sa/4.0/deed.de)


### pixel-ani-dog_lighter_Moini.png

Der [animierte kleine Hund](https://opengameart.org/content/animals-and-tea-by-strawheart) ist von http://mtsids.com, die Farbe und das Timing nachbearbeitet von Maren Hachmann.  
Lizenz: [**CC-By-SA 3.0**](https://creativecommons.org/licenses/by-sa/3.0/deed.de)


### pool_cookout.png
[Schwimmbecken, Grill, Gartenstühle, Wasserball, Teller/Besteck und einige der Lebensmittel](https://opengameart.org/content/pool-cookout) kommen von OpenGameart.  
Lizenz: [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)


### stage.png

[Bühne](https://freesvg.org/stage) von OpenClipart/FreeSVG. 
Lizenz: [**PD**](https://de.wikipedia.org/wiki/Gemeinfreiheit)

[Grunge-Textur](https://freesvg.org/vector-graphics-of-dirty-surface] ebenfalls von OpenClipArt/FreeSVG  
Lizenz: [**PD**](https://de.wikipedia.org/wiki/Gemeinfreiheit)

[SFD-Logo](https://wiki.softwarefreedomday.org/2011/Artwork/ProposedLogoMaxus) designed von Jeff Lim und Maxus Media & Software Pte. Ltd. in Singapore und der SFD-Community.  
Lizenz: [**CC-By-SA 3.0**](https://creativecommons.org/licenses/by-sa/3.0/deed.de)


### wobble-jump-sample_2-1_GPL2+3_S.Groundwater.png

[Tuxe](https://opengameart.org/content/small-tux-walk-low-bi)  gezeichnet von S. Groundwater, modifiziert von Maren Hachmann  
Doppel-Lizenz: [**GPL 2.0**](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html) und [**GPL 3.0**](https://www.gnu.org/licenses/gpl-3.0)

<h2>Kartenerstellung</h2>

Die Karte wurde zusammengestellt von Maren Hachmann, [vektorrascheln.de](https://vektorrascheln.de).  
Lizenz: [**CC-By-SA 4.0**](https://creativecommons.org/licenses/by-sa/4.0/deed.de)


<!-- 
Lizenz: [**CC-By 3.0**](https://creativecommons.org/licenses/by/3.0/deed.de)


Lizenz: [**CC-By-SA 3.0**](https://creativecommons.org/licenses/by-sa/3.0/deed.de) 


Lizenz: [**CC-By-SA 4.0**](https://creativecommons.org/licenses/by-sa/4.0/deed.de)


Lizenz: [**CC-0**](https://creativecommons.org/publicdomain/zero/1.0/deed.de)


Lizenz: <a target="_blank" href="https://de.wikipedia.org/wiki/Gemeinfreiheit"><strong><img class="cc-image" src="/static/images/publicdomain.svg"/> PD**</a>


Lizenz: [**GPL 2.0**](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)


Lizenz: [**GPL 3.0**](https://www.gnu.org/licenses/gpl-3.0) -->